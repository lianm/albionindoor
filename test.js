//
//  Test file for arranging and cleaning code
//
//  Welcome --------------------------------------------------------
//  This is my new project, allowing players from a specific social
//  sport club, to easily check their game times through facebook
//  ----------------------------------------------------------------
//

const vtoken = process.env.FB_PAGE_ACCESS_TOKEN
const token = process.env.FB_VERIFY_ACCESS_TOKEN

const cheerio = require("cheerio");
const request = require("request");
var $ = require("jquery");

//SCRAPER - Start

class TeamTime{
  constructor(time, court, date, day){
    this.time = time;
    this.date = date;
    this.court = court;
    this.day = day;
  }
}

const link_prefix = "http://results.sportskeepglobal.com/qld--albion-indoor-sports/";

function searchTeam(team_name){ //First query from messenger to start the proccess

  const formData = {
     teamNameSearch: team_name,
  };

  //Request team link from search php form
  return new Promise((resolve, reject) => {
    request.post({
      url: 'http://results.sportskeepglobal.com/qld--albion-indoor-sports/stadium-team-search-results.php',
      form: formData
    }, (err, httpResponse, body) => {
      if(!err){
        resolve(tableToData(body, formData.teamNameSearch.toLowerCase()));
      }
    });
  }).then((data) => {
    return (data)
  })
}

function tableToData(body, team_name){
  return new Promise((resolve, reject) => {
    const $ = cheerio.load(body);
    $("tr").each((row_index, row) => { //table row splitter

      var data_to_array = [];

      $(row).find("td").each((data_index, data) => { //table data splitter
        var line = $(data).text().toLowerCase(); //individual data
        data_to_array.push(line);
        if(data_index == 4){
          $(data).find("a").each((a_index, href) => {
            if(a_index == 1) resolve($(href).attr("href"));
          })
        }
      });
      console.log(data_to_array);
    })
  }).then((val) => {
    return val;
  });
}

function getTeamInformation(link){
  return new Promise((resolve, reject) => {
    request(link_prefix + link, (err, res, htm) => {
      console.log(link_prefix + link);
      var team = new TeamTime();
      const $ = cheerio.load(htm); //JQuery
      var details = []; //Details Array

      //Scrapes the table row into 'bits'
      $("tr").each((q, tr) => {
        $(tr).find("td").each((epp, bits) => {
          details.push($(bits).text()); //Moves the bits into the details array
        });
      });
      //Specifically selects the details - pattern doesnt change
      console.log(details);
      team.time = details[4]
      team.court = details[5]
      team.date = details[3]
      team.day = details[2]

      resolve(team);
    })
  }).then((data) => {
    return data;
  })
}


function find(team){
  return new Promise((resolve, reject) => {
      resolve(searchTeam(team));
  }).then((urrl) => {
      return getTeamInformation(urrl)
  })
}

new Promise((resolve, reject) => {
  resolve(find("Giants"));
}).then((data) => {
  console.log(data);
})


//END SCRAPER
