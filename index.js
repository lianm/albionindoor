'use strict'

const vtoken = process.env.FB_PAGE_ACCESS_TOKEN
const token = process.env.FB_VERIFY_ACCESS_TOKEN

const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const cheerio = require("cheerio");
const request = require("request");
var $ = require("jquery");
var artoo = require('artoo-js');
var moment = require('moment');
const app = express()

app.set('port', (process.env.PORT || 5000))

// Process application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}))

// Process application/json
app.use(bodyParser.json())

app.use(express.static(path.join(__dirname, 'public')))

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug')

// Index route
app.get('/', function (req, res) {
    res.send('Hello world, I am a chat bot')
})

// for Facebook verification
app.get('/webhook/', function (req, res) {
    if (req.query['hub.verify_token'] === vtoken) {
        res.send(req.query['hub.challenge'])
    }
    res.send('No sir')
})




//SCRAPER

class TeamTime{
  constructor(time, court, date, day){
    this.time = time;
    this.date = date;
    this.court = court;
    this.day = day;
  }
}

const link_prefix = "http://results.sportskeepglobal.com/qld--albion-indoor-sports/";

function searchTeam(team_name){ //First query from messenger to start the proccess

  const formData = {
     teamNameSearch: team_name,
  };

  //Request team link from search php form
  return new Promise((resolve, reject) => {
    request.post({
      url: link_prefix +'stadium-team-search-results.php',
      form: formData
    }, (err, httpResponse, body) => {
      if(!err){
        resolve(tableToData(body, formData.teamNameSearch.toLowerCase()));
      }
    });
  }).then((data) => {
    return (data)
  })
}

function tableToData(body, team_name){
  return new Promise((resolve, reject) => {
    const $ = cheerio.load(body);
    $("tr").each((row_index, row) => { //table row splitter

      var data_to_array = [];

      $(row).find("td").each((data_index, data) => { //table data splitter
        var line = $(data).text().toLowerCase(); //individual data
        data_to_array.push(line);
        if(data_index == 4){
          $(data).find("a").each((a_index, href) => {
            if(a_index == 1) resolve($(href).attr("href"));
          })
        }
      });
      console.log(data_to_array);
    })
  }).then((val) => {
    return val;
  });
}

function getTeamInformation(link){
  return new Promise((resolve, reject) => {
    request(link_prefix + link, (err, res, htm) => {
      console.log(link_prefix + link);
      var team = new TeamTime();
      const $ = cheerio.load(htm); //JQuery
      var details = []; //Details Array

      //Scrapes the table row into 'bits'
      $("tr").each((q, tr) => {
        $(tr).find("td").each((epp, bits) => {
          details.push($(bits).text()); //Moves the bits into the details array
        });
      });
      //Specifically selects the details - pattern doesnt change
      console.log(details);
      team.time = details[4]
      team.court = details[5]
      team.date = details[3]
      team.day = details[2]

      resolve(team);
    })
  }).then((data) => {
    return data;
  })
}


function find(team){
  return new Promise((resolve, reject) => {
      resolve(searchTeam(team));
  }).then((urrl) => {
      return getTeamInformation(urrl)
  })
}

//END SCRAPER


// Spin up the server
app.listen(app.get('port'), function() {
    console.log('running on port', app.get('port'))
})

app.post('/webhook/', function (req, res) {
    let messaging_events = req.body.entry[0].messaging
    for (let i = 0; i < messaging_events.length; i++) {
      let event = req.body.entry[0].messaging[i]
      let sender = event.sender.id
      if (event.message && event.message.text) {
        let text = event.message.text
        if (text === 'Generic') {
            sendGenericMessage(sender)
            continue
        }

        new Promise((resolve, reject) => {
          resolve(find(text));
        }).then((data) => {
          sendTextMessage(sender, data.time + " | " + data.date + " | " + data.court)
        }).catch((err) => {
          sendGenericMessage(sender)
        })
      }
      if (event.postback) {
        let text = JSON.stringify(event.postback)
        sendTextMessage(sender, "Postback: "+text.substring(0, 200), token)
        continue
      }
    }
    res.sendStatus(200)
  })

  app.get("/privacy", (req, res) => {
    res.render("index")
  })

  const sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }

function sendTextMessage(sender, text) {
    let messageData = { text:text }
    request({
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {access_token:token},
        method: 'POST',
        json: {
            recipient: {id:sender},
            message: messageData,
        }
    }, function(error, response, body) {
        if (error) {
            console.log('Error sending messages: ', error)
        } else if (response.body.error) {
            console.log('Error: ', response.body.error)
        }
    })
}

function sendGenericMessage(sender) {
    let messageData = {
        "attachment": {
            "type": "template",
            "payload": {
                "template_type": "generic",
                "elements": [{
                    "title": "Having trouble?",
                    "subtitle": "Check out our website!",
                    "image_url": "http://results.sportskeepglobal.com/qld--albion-indoor-sports/images/stadium-logos/8350.jpg",
                    "buttons": [{
                        "type": "web_url",
                        "url": "http://results.sportskeepglobal.com/qld--albion-indoor-sports/",
                        "title": "Website"
                    }]
                }, {
                    "title": "Second card",
                    "subtitle": "Element #2 of an hscroll",
                    "image_url": "http://messengerdemo.parseapp.com/img/gearvr.png",
                    "buttons": [{
                        "type": "postback",
                        "title": "Postback",
                        "payload": "Payload for second element in a generic bubble",
                    }],
                }]
            }
        }
    }
    request({
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {access_token:token},
        method: 'POST',
        json: {
            recipient: {id:sender},
            message: messageData,
        }
    }, function(error, response, body) {
        if (error) {
            console.log('Error sending messages: ', error)
        } else if (response.body.error) {
            console.log('Error: ', response.body.error)
        }
    })
}
